FRENCH ONLY
===========

Ce projet est déstiné à l'équipe **Raydio**, webradio du lycée Raynouard dont le site web actuel est accessible [ici](https://webmedias.ac-nice.fr/raydio/).

Développeur : Alexy **DUTOIS-RUIZ**

**SCREENSHOT**

![Screenshot_Accueil](https://gitlab.com/Al.D-R/raydio-website/raw/master/public/img/Screenshot.png)

**SCREENSHOT MOBILE**

![Screenshot_Accueil](https://gitlab.com/Al.D-R/raydio-website/raw/master/public/img/Screenshot_mobile.png)
