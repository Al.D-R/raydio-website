@extends('layout')

@section('content')
<div class="inscription">
    <form method="POST" action="inscription.php">
        <p><label for="name">Nom : </label><input name="name" type="text" placeholder="Dumont"/></p>
        <p><label for="surname">Prénom : </label><input name="surname" type="text" placeholder="Michel"/></p>
        <p><label for="age">Age : </label><input name="age" type="number"/></p>
        <p><label for="email">Adresse Email : </label><input name="email" type="email" placeholder="michel.dumont@netc.fr"/></p>
        <p><label for="tel">Numéro de téléphone : </label><input name="tel" type="tel"/></p>
        <p><label for="newsletter">Recevoir notre newsletter : </label><input name="newsletter" type="checkbox"/></p>
        <p><label for="accepter">J'accepte les conditions d'utilisation : </label><input name="accepter" type="checkbox"/></p>
        <p><input name="submit" type="submit" id="submit"/></p>
    </form>
</div>
@endsection
