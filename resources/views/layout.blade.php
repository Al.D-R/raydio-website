<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Raydio </title>
        <!--<link rel="stylesheet" href="./../../public/css/style.css" />-->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link rel="shorcut icon" href=" {{ asset('img/logo-raydio.png') }} ">
    </head>

    <body>

	      <!-- Menu Mobile (Faible resolution) -->
        <div class="menuMobile">
            <div class="menuModileImg" id="menuMobileIcon">
	              <a href="#"> <img src="./img/menu-icon.png" /> </a>
            </div>
            <div class="menuModileImg">
	              <img src="./img/logo-raydio.png" />
            </div>
        </div>

	      <!-- Banniere -->
        <div id="haut" class="banner"><img src=" {{ asset('img/banniere-raydio.png') }} " /></div>


	      <!-- Menu Desktop -->
        <div class="menu">
            <div class="home-icon"> <a href="index.php"> <img src=" {{ asset('img/home-icon.png') }} "/> </a> </div>
            <a href="./../index.php" id="active"> <div class="element"> <p> Accueil </p> </div> </a>
            <a href=""> <div > <p> Nos reportages </p> </div> </a>
            <a href=""> <div > <p> Nos podcasts </p> </div> </a>
            <a href=""> <div > <p> Notre equipe </p> </div> </a>
            <a href="/inscription"> <div > <p> Inscription </p> </div> </a>
            <a href=""> <div > <p> Connection </p> </div> </a>
            <div class="icon"> <a href="https://twitter.com/RaydioRaynouard"> <img src=" {{ asset('img/twitter-icon.png') }} "/> </a> </div>
        </div>


	      <!-- Articles + Widgets -->
        <div class="page">


	          <!-- Widgets -->
	          <div class="sidebar">
                <div class="player"> <iframe src="https://www.radioking.com/widgets/player/player.php?id=44347&c=%231E7FCB&c2=%23FFFFFF&ii=&p=1&pp=1&i=1&eq=1&po=1&t=1&f=v&v=2&s=1&li=1&h=365&l=470&a=0&plc=0&popup=1" name="idFrame" id="idFrame" style="border-radius: 5px; width:275px;height:365px; min-height: 365px;" frameBorder="0"></iframe>
                </div>

                <div class="discord"> <iframe src="https://discordapp.com/widget?id=490868942013988864&theme=dark" width="280" height="500" allowtransparency="true" frameborder="0"></iframe> 
                </div>
	          </div>

            @yield('content')

        </div>


	      <!-- Footer -->
        <div class="footer">
            <footer>
	              <p>
		                Copyright, Alexy DUTOIS-RUIZ, tous droits réservés
	              </p>
            </footer>
        </div>


	      <!-- Fleche remonte haut de la page -->
        <div> <a href="#haut"> <img class="fleche" src=" {{ asset('img/up-icon.png') }} " /> </a> </div>


	      <!-- Inclusion de Javascript -->
        <!--<script src=" {{ asset('js/app.js') }} " ></script>-->

    </body>
</html>
